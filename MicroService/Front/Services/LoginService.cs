﻿using Front.Entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.Net.WebSockets;
using System.Runtime.CompilerServices;
using System.Security.Claims;


namespace Front.Services
{
    public class LoginService
    {
        private readonly HttpClient _httpClient;

        public LoginService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<UserAndToken> AuthenticateUser(string username, string password)
        {
            if (_httpClient.BaseAddress == null)
            {   
                _httpClient.BaseAddress = new Uri("http://localhost:5000/");
             }
           UserLogin user = new UserLogin { Name = username, Pass = password };
            var login = await _httpClient.PostAsJsonAsync("api/User/login", user);
            if (login.IsSuccessStatusCode)
            {
              var result = await login.Content.ReadFromJsonAsync<UserAndToken>();
                return result;
            }
            else
            {
                
                Console.WriteLine("Login failed");
                return null;
            }

        }
        public async Task<UserCreateModel> RegisterUser(string username, string password, string mail)
        {
            if(_httpClient.BaseAddress == null)
                _httpClient.BaseAddress = new Uri("http://localhost:5000/");
            UserCreateModel user = new UserCreateModel { Name = username, Password = password, Email = mail };
            var register = await _httpClient.PostAsJsonAsync("api/User/register", user);
            if (register.IsSuccessStatusCode)
            {
                var result = await register.Content.ReadFromJsonAsync<UserCreateModel>();
                return result;
            }
            else
            {
                Console.WriteLine("Login failed");
                return null;
            }

        }

    }
}

